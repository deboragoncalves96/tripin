﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tripin.Models
{
    public class Rota
    {
        public int RotaID { get; set; }
        public string nome_rota { get; set; }
        public double lat1_rota { get; set; }
        public double lat2_rota { get; set; }
        public double lat3_rota { get; set; }
        public double lat4_rota { get; set; }
        public double lat5_rota { get; set; }
        public double lon1_rota { get; set; }
        public double lon2_rota { get; set; }
        public double lon3_rota { get; set; }
        public double lon4_rota { get; set; }
        public double lon5_rota { get; set; }
    }
}