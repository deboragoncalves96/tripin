﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tripin.Models
{
    public class Ponto
    {
        public int PontoID { get; set; }
        public string nome_ponto { get; set; }
        public string tipo_ponto { get; set; }
        public string descricao_ponto { get; set; }
        public decimal latitude_ponto { get; set; }
        public decimal longitude_ponto { get; set; }
        public string localidade { get; set; }
        public string url { get; set; }
    }
}