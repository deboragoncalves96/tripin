// <auto-generated />
namespace TripIn.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class displayformat : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(displayformat));
        
        string IMigrationMetadata.Id
        {
            get { return "201607052147385_displayformat"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
