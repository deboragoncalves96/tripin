﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tripin.DAL;

namespace tripin.Controllers
{
    public class HomeController : Controller
    {
        private TripinDbContext db = new TripinDbContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ViladoConde()
        {
            return View();
        }

        public ActionResult PovoadeVarzim()
        {
            //Buscar Pontos de Póvoa de Varzim
            var pontos = from m in db.Pontos.Where(p => p.localidade == "Póvoa de Varzim")
                         select m;

            return View(pontos);
        }

        public ActionResult VisitaVirtual()
        {
            return View();
        }
    }
}